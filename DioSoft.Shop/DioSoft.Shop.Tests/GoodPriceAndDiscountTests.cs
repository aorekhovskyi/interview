﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DioSoft.Shop.Goods;
using DioSoft.Shop.Goods.Impl;
using Microsoft.Practices.Unity;
using NUnit.Framework;

namespace DioSoft.Shop.Tests
{
    [TestFixture]
    public class GoodPriceAndDiscountTests
    {
        #region Fields

        private Dictionary<string, int> valuesA;
        private Dictionary<string, int> valuesB;
        private Dictionary<string, int> valuesC;

        private IUnityContainer container;

        #endregion Fields

        #region Initialization

        [SetUp]
        public void Initialize()
        {
            this.valuesA = new Dictionary<string, int>
            {
                {new string('A', 1), 10}, {new string('A', 3), 10},
                {new string('A', 4), 9}, {new string('A', 7), 9},
                {new string('A', 8), 8},{new string('A', 100), 8}
            };

            this.valuesB = new Dictionary<string, int>
            {
                {new string('B', 1), 20}, {new string('B', 5), 20},
                {new string('B', 6), 16},{new string('B', 100), 16}
            };

            this.valuesC = new Dictionary<string, int>
            {
                {new string('C', 1), 6}, {new string('C', 5), 6},
                {new string('A', 5), 6}, {new string('C', 100), 6}
            };

            this.container = new UnityContainer();
            this.container
                .RegisterType<IGoodFactory, GoodFactory>()
                .RegisterType<ISalesStrategyEngine, SalesStrategyEngine>();
        }

        #endregion Initialization

        #region Test

        [Test]
        public void GoodAPriceCalculatingTest()
        {            
            var goodTypeA = this.container.Resolve<IGoodFactory>().CreateA();
            this.PriceTest(goodTypeA, valuesA);
        }


        [Test]
        public void GoodBPriceCalculatingTest()
        {
            var goodTypeB = this.container.Resolve<IGoodFactory>().CreateB();
            this.PriceTest(goodTypeB, valuesB);
        }

        [Test]
        public void GoodCPriceCalculatingTest()
        {
            var goodTypeC = this.container.Resolve<IGoodFactory>().CreateC();
            this.PriceTest(goodTypeC, valuesC);
        }

        [Test]
        public void AllGoodsInCartDiscountTest()
        {
            var engine = this.container.Resolve<ISalesStrategyEngine>();            

            var discount = engine.GetDiscountForCart(new char[] { 'A', 'B' });
            Assert.AreEqual(discount, 1);

            discount = engine.GetDiscountForCart(new char[] { 'A', 'C' });
            Assert.AreEqual(discount, 1);

            discount = engine.GetDiscountForCart(new char[] { 'A', 'B', 'C' });
            Assert.AreEqual(discount, 0.95m);
        }

        private void PriceTest(IGood good, Dictionary<string, int> values)
        {
            var ruleEngine = this.container.Resolve<ISalesStrategyEngine>();
            
            foreach (var val in values)
            {
                var price = ruleEngine.GetPriceForGood(val.Key.ToCharArray(), good);                    

                Assert.AreEqual(price, val.Value,
                    string.Format("Incorrect price for good with type '{0}' and count: {1}", good.Code,
                        val.Key.Count()));
            }
        }

        #endregion Tests
    }
}