﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using DioSoft.Shop.Calculator;
using DioSoft.Shop.Calculator.Impl;
using DioSoft.Shop.Goods;
using DioSoft.Shop.Goods.Impl;
using Microsoft.Practices.Unity;
using Moq;
using NUnit.Framework;

namespace DioSoft.Shop.Tests
{
    [TestFixture]
    public class CalculatingTotalTests
    {
        private IUnityContainer container;
        private Dictionary<string, decimal> testData;

        [SetUp]
        public void Init()
        {
            this.container = new UnityContainer();            
            
            this.container
                .RegisterType<ICartTotalCalculator, CartTotalCalculator>()
                .RegisterType<ISalesStrategyEngine, SalesStrategyEngine>()
                .RegisterType<IGoodFactory, GoodFactory>();

            this.ConfigureTestData();
        }

        [Test]
        public void CalculateTotalTest()
        {
            var calculator = this.container.Resolve<ICartTotalCalculator>();
            foreach (var val in testData)
            {
                Assert.AreEqual(val.Value, calculator.Calculate(val.Key.ToCharArray()));
            }
        }

        private void ConfigureTestData()
        {
            this.testData = new Dictionary<string, decimal>();

            testData.Add("A", 10);
            testData.Add("BBBBBB",96);
            testData.Add("CC", 12);
            testData.Add("ABC", 34.2m);
            testData.Add("ABBCAACBBBA", 140.6m);
            testData.Add("AABAA", 56);
        }
    }
}