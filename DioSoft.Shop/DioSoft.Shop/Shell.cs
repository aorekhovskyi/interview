﻿using System;
using DioSoft.Shop.Calculator;
using DioSoft.Shop.Calculator.Impl;
using DioSoft.Shop.Goods;
using DioSoft.Shop.Goods.Impl;
using Microsoft.Practices.Unity;

namespace DioSoft.Shop
{
    public static class Shell
    {
        public static void Run(IUnityContainer container)
        {
            ConfigureUnity(container);

            var calculator = container.Resolve<ICartTotalCalculator>();

            do
            {
                Console.WriteLine("Enter your cart ('q' for exit):");
                var cart = Console.ReadLine();
                if (cart != null && !cart.ToUpper().Equals("Q"))
                {
                    Console.WriteLine("Total:{0}", calculator.Calculate(cart.ToCharArray()));
                }
                else
                {
                    break;
                }
            } while (true);  
        }

        private static void ConfigureUnity(IUnityContainer container)
        {
            container
                .RegisterType<ICartTotalCalculator, CartTotalCalculator>()
                .RegisterType<ISalesStrategyEngine, SalesStrategyEngine>()
                .RegisterType<IGoodFactory, GoodFactory>();
        }
    }
}