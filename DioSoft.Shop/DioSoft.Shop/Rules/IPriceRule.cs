﻿using System;
using System.Collections.Generic;

namespace DioSoft.Shop.Rules
{
    public interface IPriceRule
    {
        Func<ICollection<char>,bool> CheckCondition { get; }

        Func<ICollection<char>, decimal> Calculate { get; }
    }
}