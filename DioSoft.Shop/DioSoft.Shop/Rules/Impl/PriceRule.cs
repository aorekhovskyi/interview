﻿using System;
using System.Collections.Generic;

namespace DioSoft.Shop.Rules.Impl
{
    public class PriceRule : IPriceRule
    {
        public Func<ICollection<char>,bool> CheckCondition { get; private set; }
        public Func<ICollection<char>, decimal> Calculate { get; private set; }

        public PriceRule(Func<ICollection<char>, bool> checkCondition, Func<ICollection<char>, decimal> calculate)
        {
            this.CheckCondition = checkCondition;
            this.Calculate = calculate;
        }
    }
}