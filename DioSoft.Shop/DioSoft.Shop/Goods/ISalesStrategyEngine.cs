﻿using System.Collections.Generic;

namespace DioSoft.Shop.Goods
{
    public interface ISalesStrategyEngine
    {
        ICollection<IGood> ShopProductLines { get; }

        decimal GetPriceForGood(char[] cartCode, IGood good);

        decimal GetDiscountForCart(char[] cartCode);
        
    }
}