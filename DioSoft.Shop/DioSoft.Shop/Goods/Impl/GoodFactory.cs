﻿using System.Linq;
using DioSoft.Shop.Rules.Impl;

namespace DioSoft.Shop.Goods.Impl
{
    public class GoodFactory : IGoodFactory
    {
        public IGood CreateA()
        {
            var goodA = new Good('A');

            goodA.AddPriceRule(new PriceRule(
                (c) => c.Count(e => e.Equals('A')) >= 1 && c.Count(e => e.Equals('A')) < 4, (c) => 10));

            goodA.AddPriceRule(
                new PriceRule((c) => c.Count(e => e.Equals('A')) >= 4 && c.Count(e => e.Equals('A')) < 8, (c) => 9));

            goodA.AddPriceRule(new PriceRule((c) => c.Count(e => e.ToString().ToUpper().Equals("A")) >= 8, (c) => 8));

            return goodA;
        }

        public IGood CreateB()
        {
            var goodB = new Good('B');

            goodB.AddPriceRule(
                new PriceRule((c) => c.Count(e => e.Equals('B')) >= 1 && c.Count(e => e.Equals('B')) < 6, (c) => 20));

            goodB.AddPriceRule(new PriceRule((c) => c.Count(e => e.Equals('B')) >= 6, (c) => 16));

            return goodB;
        }

        public IGood CreateC()
        {
            var goodC = new Good('C');

            goodC.AddPriceRule(new PriceRule((c) => true, (c) => 6));

            return goodC;
        }
    }
}