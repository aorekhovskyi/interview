﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using DioSoft.Shop.Rules;

namespace DioSoft.Shop.Goods.Impl
{
    public class Good : IGood
    {
        #region Properties

        public char Code { get; private set; }

        public ICollection<IPriceRule> Rules { get; private set; }

        #endregion Properties

        #region Contructor

        public Good(char code)
        {
            this.Code = code;
            this.Rules = new Collection<IPriceRule>();
        }

        #endregion Contructor

        #region Methods

        public void AddPriceRule(IPriceRule rule)
        {
            if (!this.Rules.Contains(rule))
            {
                this.Rules.Add(rule);
            }
        }

        #endregion Methods
    }
}