﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Practices.Unity;

namespace DioSoft.Shop.Goods.Impl
{
    public class SalesStrategyEngine : ISalesStrategyEngine
    {
        #region Fields

        private ICollection<IGood> shopProductLines;

        #endregion Fields

        #region Properties

        [Dependency]
        public IGoodFactory GoodFactory { get; set; }

        public ICollection<IGood> ShopProductLines
        {
            get
            {
                return this.shopProductLines ?? (this.shopProductLines = new Collection<IGood>
                {
                    GoodFactory.CreateA(),
                    GoodFactory.CreateB(),
                    GoodFactory.CreateC()
                });
            }
        }

        #endregion Properties

        #region Implementation

        public decimal GetPriceForGood(char[] cartCode, IGood good)
        {
            var priceRule = good.Rules.FirstOrDefault(p => p.CheckCondition(cartCode));
            return priceRule == null ? 0 : priceRule.Calculate(cartCode);
        }

        public decimal GetDiscountForCart(char[] cartCode)
        {
            return
                this.ShopProductLines.Any(goodType => !cartCode.Contains(goodType.Code)) ? 1 : 0.95m;
        }

        #endregion Implementation
    }
}