﻿namespace DioSoft.Shop.Goods
{
    public interface IGoodFactory
    {
        IGood CreateA();
        IGood CreateB();
        IGood CreateC();
    }
}