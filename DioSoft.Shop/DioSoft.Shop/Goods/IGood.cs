﻿using System.Collections.Generic;
using DioSoft.Shop.Rules;

namespace DioSoft.Shop.Goods
{
    public interface IGood
    {
        char Code { get; }

        ICollection<IPriceRule> Rules { get; }

        void AddPriceRule(IPriceRule rule);
    }
}