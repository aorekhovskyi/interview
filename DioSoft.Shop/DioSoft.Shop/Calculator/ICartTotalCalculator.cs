﻿using DioSoft.Shop.Rules;

namespace DioSoft.Shop.Calculator
{
    public interface ICartTotalCalculator
    {
        decimal Calculate(char[] cartCode);
    }
}