﻿using System.Linq;
using DioSoft.Shop.Goods;
using Microsoft.Practices.Unity;

namespace DioSoft.Shop.Calculator.Impl
{
    public class CartTotalCalculator : ICartTotalCalculator
    {

        [Dependency]
        public ISalesStrategyEngine SalesStrategyEngine { get; set; }

        public decimal Calculate(char[] cartCode)
        {
            var total = (from good in this.SalesStrategyEngine.ShopProductLines
                let price = this.SalesStrategyEngine.GetPriceForGood(cartCode, good)
                select cartCode.Where(c => c.Equals(good.Code)).Count()*price).Sum();

            return total * this.SalesStrategyEngine.GetDiscountForCart(cartCode);
        }
    }
}