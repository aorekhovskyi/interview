﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace DioSoft.Shop
{    
    class Program
    {
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            try
            {
                Shell.Run(container);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Application error:{0}", ex.Message);
            }

        }
    }    
}
